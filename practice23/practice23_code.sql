/*SQL Practice 23:- Co-related Queries*/

SELECT * FROM customers
WHERE id = ANY (SELECT customer_id FROM orders
WHERE customers.city = orders.ship_city);
