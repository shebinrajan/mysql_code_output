/*SQL Practice 15:- Show all records from matching or unmatching. (FULL outer join).*/

SELECT city FROM customers
UNION ALL
SELECT city FROM employees;
