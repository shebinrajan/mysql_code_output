/*SQL Practice 18:- Display aggregate values from a table (GROUP BY)*/

SELECT COUNT(id) AS count, city, first_name, last_name
FROM customers
GROUP BY city;
