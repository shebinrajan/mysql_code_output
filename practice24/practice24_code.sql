/*SQL Practice 24:- Find Max, Min and Average.

MIN*/

SELECT MIN(standard_cost) AS SmallestPrice
FROM products;

/*MAX*/

SELECT MAX(standard_cost) AS SmallestPrice
FROM products;

/*AVG*/

SELECT AVG(shipping_fee)
FROM orders;
