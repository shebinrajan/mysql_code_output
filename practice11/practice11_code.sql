/*SQL Practice 11:- CASE statements with SQL.*/

SELECT product_id, purchase_order_id, 
(CASE WHEN quantity < 50 THEN 'average' 
WHEN quantity >50 THEN 'good'
END) AS state
FROM order_details;
