/*SQL Practice 6:- Sort data using descending.*/
SELECT id, first_name,  last_name
FROM customers 
ORDER BY first_name
DESC;
