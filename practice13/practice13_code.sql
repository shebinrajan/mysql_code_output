/*SQL Practice 13:- Show matching data from two tables.( Inner Join)*/

SELECT orders.id AS order_id, orders.shipped_date, customers.first_name, customers.last_name, customers.address
FROM customers
INNER JOIN orders
ON orders.customer_id= customers.id;
