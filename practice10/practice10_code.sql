/*SQL Practice 10:- Create runtime calculated columns.*/


SELECT product_code, product_name, standard_cost, list_price, (list_price - standard_cost) 
AS difference 
FROM products;