/*SQL Practice 20:- Self Join*/

SELECT purchase_orders.supplier_id, purchase_orders.shipping_fee, suppliers.company, suppliers.state_province
FROM purchase_orders
JOIN suppliers
ON suppliers.id=purchase_orders.supplier_id;
