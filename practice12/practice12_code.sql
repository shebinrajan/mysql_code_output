/*SQL Practice 12:- Join data from two SELECTS using UNION and UNION ALL.

UNION*/

SELECT city FROM customers
UNION 
SELECT city FROM employees;

/*UNION ALL*/

SELECT city FROM customers
UNION ALL
SELECT city FROM employees;
