/*SQL Practice 9:- Searching using pattern and wildcards.

starts with s*/

SELECT first_name, last_name 
FROM employees 
WHERE first_name 
LIKE 's%';

/*contain with sales*/

SELECT company, job_title 
FROM suppliers
WHERE job_title 
LIKE '%sales%';

/*ends with n*/

SELECT first_name, last_name 
FROM employees 
WHERE first_name 
LIKE '%n';

/*contain only 4 letters and starts with J*/

SELECT first_name, last_name 
FROM customers 
WHERE first_name
LIKE 'J___';
