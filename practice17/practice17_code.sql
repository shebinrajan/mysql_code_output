/*SQL Practice 17:- Writing a complex SQL inner join statement.*/

SELECT order_details.order_id, order_details.product_id, products.product_name, products.standard_cost 
FROM order_details
INNER JOIN orders ON orders.id=order_details.order_id 
INNER JOIN products ON products.id=order_details.product_id;
