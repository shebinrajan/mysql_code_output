/*SQL Practice 19:- Filter on Aggregate values (HAVING CLAUSE)*/

SELECT order_id, SUM(unit_price * quantity) Total
FROM order_details
GROUP BY order_id
HAVING total > 1000;
