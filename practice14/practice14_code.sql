/*SQL Practice 14:- Show all records from one table and only matching record from other table. ( Left and Right)

Left join*/

SELECT orders.id AS order_id, orders.shipped_date, customers.first_name, customers.last_name, customers.address
FROM customers
LEFT JOIN orders
ON orders.customer_id= customers.id;

/*Right join*/

SELECT orders.id AS order_id, orders.shipped_date, customers.first_name, customers.last_name, customers.address
FROM customers
RIGHT JOIN orders
ON orders.customer_id= customers.id;
