/*SQL Practice 16:- Show cartersian of two tables( CROSS JOIN)*/

SELECT orders_status.status_name AS orders_status, orders_tax_status.tax_status_name AS orders_tax_status
FROM orders_status
CROSS JOIN orders_tax_status;
