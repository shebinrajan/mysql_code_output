/*SQL Practice 21:- ISNULL*/

SELECT order_id, product_id, quantity, purchase_order_id
FROM order_details
WHERE purchase_order_id IS NULL;
